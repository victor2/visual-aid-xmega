# README #

Connectors on XMEGA board:

* pin 6 - trigger output, to be connected to the trigger input of sound module
* pin 5 - ADC input, to be connected to the analog output of sound module ()
* pin 4 - DAC output, to be connected to an amplifier or possibly a headphone
* pin 9 - ground