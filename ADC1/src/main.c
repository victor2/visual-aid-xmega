/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/**
 * \mainpage User Application template doxygen documentation
 *
 * \par Empty user application template
 *
 * Bare minimum empty user application template
 *
 * \par Content
 *
 * -# Include the ASF header files (through asf.h)
 * -# "Insert system clock initialization code here" comment
 * -# Minimal main function that starts with a call to board_init()
 * -# "Insert application code here" comment
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */
#include <asf.h>

#define TRIGGER_PIN    IOPORT_CREATE_PIN(PORTA, 5)
#define MY_ADC    ADCB
#define MY_ADC_CH ADC_CH1

#define SAMPLING_RATE	50000UL
#define NUMBER_OF_SAMPLES 2000
#define STRETCH_RATIO	96

#define SPEAKER_DAC            DACB
//! \note This is the left channel of Xplain's on-board amplifier.
#define SPEAKER_DAC_CHANNEL    DAC_CH0


adc_result_t adc_samples[NUMBER_OF_SAMPLES];
uint16_t adc_index = 0;
uint16_t dac_index = 0;

#define SINE_DIM 32

static const uint16_t sine[SINE_DIM] = {
	32768, 35325, 37784, 40050, 42036, 43666, 44877, 45623,
	45875, 45623, 44877, 43666, 42036, 40050, 37784, 35325,
	32768, 30211, 27752, 25486, 23500, 21870, 20659, 19913,
	19661, 19913, 20659, 21870, 23500, 25486, 27752, 30211,
};

void dac_init(void);

static void adc_init(void)
{
	struct adc_config adc_conf;
	struct adc_channel_config adcch_conf;
	adc_read_configuration(&MY_ADC, &adc_conf);
	adcch_read_configuration(&MY_ADC, MY_ADC_CH, &adcch_conf);
	adc_set_conversion_parameters(&adc_conf, ADC_SIGN_ON, ADC_RES_12, ADC_REF_VCC);
	adc_set_conversion_trigger(&adc_conf, ADC_TRIG_MANUAL, 1, 0);
	adc_set_clock_rate(&adc_conf, SAMPLING_RATE);
	adcch_set_input(&adcch_conf, ADCCH_POS_PIN1, ADCCH_NEG_NONE, 1);
	adc_write_configuration(&MY_ADC, &adc_conf);

	adcch_write_configuration(&MY_ADC, MY_ADC_CH, &adcch_conf);
}

static void evsys_init(void)
{
	sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_EVSYS);
	EVSYS.CH3MUX = EVSYS_CHMUX_TCC0_OVF_gc;
}

static void tc_init(void)
{
	tc_enable(&TCC0);
	tc_set_wgm(&TCC0, TC_WG_NORMAL);
	tc_write_period(&TCC0, (sysclk_get_per_hz() / SAMPLING_RATE) - 1);
	tc_write_clock_source(&TCC0, TC_CLKSEL_DIV1_gc);
}

int main (void)
{
	board_init();
	sysclk_init();
	adc_init();
	evsys_init();
	tc_init();
	dac_init();
	
	ioport_init();
	ioport_enable_pin(TRIGGER_PIN);
	ioport_set_pin_dir(TRIGGER_PIN, IOPORT_DIR_OUTPUT);

	/* Insert application code here, after the board has been initialized. */
	adc_enable(&MY_ADC);
	dac_enable(&SPEAKER_DAC);
	
	while (1)
	{
		ioport_set_pin_level(TRIGGER_PIN, true);
		delay_ms(10);
		ioport_set_pin_level(TRIGGER_PIN, false);

		for (adc_index = 0; adc_index < NUMBER_OF_SAMPLES; adc_index++) {
			adc_start_conversion(&MY_ADC, MY_ADC_CH);
			adc_wait_for_interrupt_flag(&MY_ADC, MY_ADC_CH);
			adc_samples[adc_index] = (adc_get_result(&MY_ADC, MY_ADC_CH)) << 4;
		}
		
		for (uint8_t i = 0; i < STRETCH_RATIO; i++)
		{
			dac_wait_for_channel_ready(&SPEAKER_DAC, SPEAKER_DAC_CHANNEL);
			dac_set_channel_value(&SPEAKER_DAC, SPEAKER_DAC_CHANNEL, sine[i % SINE_DIM]);
		}

		for (uint16_t dac_index = 0; dac_index < NUMBER_OF_SAMPLES; dac_index++) {
			for (uint8_t i = 0; i < STRETCH_RATIO; i++)
			{
				dac_wait_for_channel_ready(&SPEAKER_DAC, SPEAKER_DAC_CHANNEL);
				dac_set_channel_value(&SPEAKER_DAC, SPEAKER_DAC_CHANNEL, adc_samples[dac_index]); // * sine[i % SINE_DIM]);
//				dac_set_channel_value(&SPEAKER_DAC, SPEAKER_DAC_CHANNEL, i ? adc_samples[dac_index] : v);
			}
		}
	}
}

void dac_init()
{
	struct dac_config conf;
	// Initialize the dac configuration.
	dac_read_configuration(&SPEAKER_DAC, &conf);

	/* Create configuration:
	 * - 1V from bandgap as reference, left adjusted channel value
	 * - one active DAC channel, no internal output
	 * - conversions triggered by event channel 0
	 * - 1 us conversion intervals
	 */
	dac_set_conversion_parameters(&conf, DAC_REF_AVCC, DAC_ADJ_LEFT);
	dac_set_active_channel(&conf, SPEAKER_DAC_CHANNEL, 0);
	//dac_set_conversion_trigger(&conf, 0, 0);
	dac_set_conversion_trigger(&conf, SPEAKER_DAC_CHANNEL, 3);
#if XMEGA_DAC_VERSION_1
	dac_set_conversion_interval(&conf, 1);
#endif
	dac_write_configuration(&SPEAKER_DAC, &conf);
	dac_enable(&SPEAKER_DAC);
	
#if 1
// not using sysclk for DAC
#if XMEGA_E
	// Configure timer/counter to generate events at sample rate.
	sysclk_enable_module(SYSCLK_PORT_C, SYSCLK_TC4);
	TCC4.PER = (sysclk_get_per_hz() / SAMPLING_RATE) - 1;

	// Configure event channel 0 to generate events upon T/C overflow.
	sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_EVSYS);
	EVSYS.CH0MUX = EVSYS_CHMUX_TCC4_OVF_gc;

	// Start the timer/counter.
	TCC4.CTRLA = TC45_CLKSEL_DIV1_gc;
#else
	// Configure timer/counter to generate events at sample rate.
	sysclk_enable_module(SYSCLK_PORT_C, SYSCLK_TC0);
	TCC0.PER = (sysclk_get_per_hz() / SAMPLING_RATE) - 1;

	// Configure event channel 0 to generate events upon T/C overflow.
	sysclk_enable_module(SYSCLK_PORT_GEN, SYSCLK_EVSYS);
	EVSYS.CH0MUX = EVSYS_CHMUX_TCC0_OVF_gc;

	// Start the timer/counter.
	TCC0.CTRLA = TC_CLKSEL_DIV1_gc;
#endif

#endif
	
}

